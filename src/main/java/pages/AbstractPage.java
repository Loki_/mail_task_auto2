package pages;

import org.openqa.selenium.WebDriver;

/**
 * Created by Loki_ on 22-Sep-16.
 */
public class AbstractPage {
    public static WebDriver driver;

    public AbstractPage(WebDriver driver){
        this.driver = driver;
    }

    protected WebDriver getDriver() {
        return driver;
    }
}
