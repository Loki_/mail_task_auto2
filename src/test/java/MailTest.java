import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.AbstractPage;
import pages.MailPage;

/**
 * Created by anna on 01.11.16.
 */
public class MailTest extends BaseTest {
    @Test(dependsOnMethods = "LoginTest.loginTest")
    private void checkMail(){
        MailPage mailPage = new MailPage(AbstractPage.driver);
        int actualResult = mailPage.getMailCount();
        Assert.assertNotEquals(actualResult, 0, "The E-mail has no letters");
        mailPage.mailLogout();
    }
}
